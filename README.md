# Prerequisite #

Systems may require proprietry drivers for hardware to function correctly (i.e. wireless drivers, video card drivers, etc.). 3rd party and proprietary drivers can be installed during the regular Ubuntu OS install. Check the Ubuntu Hardware Compatibility list for your system make/model: [Certified Ubuntu Hardware](https://ubuntu.com/certified). If the system is not listed, Ubuntu OS may still work however some hardware may not function as intended.

# Installation #

### Option 1: Installation using Session Manager ISO ###
* Download the [Session Manager Ubuntu 20.04 LTS ISO](https://tfpl.sfo2.digitaloceanspaces.com/session-manager/Ubuntu-20.04-LTS/ubuntu-20.04.3-TFPL-Session-Manager-desktop-amd64.iso) (2.88GB) MD5: [a319a77b8ff2eea5de6413ed5a12b7b0](https://tfpl.sfo2.digitaloceanspaces.com/session-manager/Ubuntu-20.04-LTS/ubuntu-20.04.3-TFPL-Session-Manager-desktop-amd64.iso)
* Create a bootable USB drive using [Rufus](https://rufus.ie)
* Boot the client system to the bootable USB drive and continue with either a normal or minimal installation of Ubuntu 20.04 LTS. 
* During the installation an initial administrative user account and password will be created. This can be any name and any password. Retain these credentials for future use.
* When the OS installation is complete, login to the administrative account, open a terminal window (Ctrl-Alt-T), and run the following command: 
```bash
cd ~ && sudo tfpl-session-manager-install
```

### Option 2: Installation using Ubuntu 20.04 LTS ISO ###

* Download the [Ubuntu 20.04 LTS ISO](https://ubuntu.com/download/desktop/thank-you?version=20.04.3&architecture=amd64)
* Create a bootable USB drive using [Rufus](https://rufus.ie)
* Boot the client system to the bootable USB drive and continue with either a normal or minimal installation of Ubuntu 20.04 LTS. 
* During the installation an initial administrative user account and password will be created. This can be any name and any password. Retain these credentials for future use.
* When the OS installation is complete, login to the administrative account, open a terminal window (Ctrl-Alt-T), and run the following command: 
```bash
cd ~ && sudo apt install git -y && git clone https://tfpl@bitbucket.org/tfplpubliccomputing/publicclientsetup.git && cd publicclientsetup && sudo bash setup
```

For system requirements and installation help please refer to the Ubuntu Documenation: [https://ubuntu.com/tutorials/install-ubuntu-desktop#1-overview](https://ubuntu.com/tutorials/install-ubuntu-desktop#1-overview)

# Post Installation Setup #

The settings.ini file will open automatically after the installation is completed. Edit this file to reflect your environment. You will need the Session Server IP address and any 5 digit barcode prefixes.

Note:
If you need to manually open the file use the following:
```bash
sudo gedit /home/guest/.session-manager/settings.ini
```

Edit /home/guest/.session-manager/settings.ini to represent your environment:
```ini
[session]
SessionServer = <Your session servers IP address>
allowedPrefixes = <5 digit barcode prefixes. If the 14 digit barcode is 1234567890123 the prefix would be 12345>
expressPrefix = 90000
```

Example settings.ini file:
```ini
[session]
SessionServer = 192.168.1.15
allowedPrefixes = 21125, 21126, 21127
expressPrefix = 90000
```

# System Accounts #

There are two accounts, the default administrative account setup during the OS installation and a guest account. The guest account will automatically login. The default password for the guest account is "timelimit". It's recommened to change this password. Use the following command to change the guest account password from a terminal window (ctrl-alt-t):
```bash
sudo passwd guest

```

# Post Installation Considerations #

* Change the default passwords
```bash
sudo passwd guest
```
* Install desired applications
```bash
sudo apt install <package name>

```

* Set a default home page for web browsers.
* Launch Libreoffice and resolve the initial startup dialog popups.
* Install DVD codecs for VLC player.
* Install printers
* Connect the system to a WiFi network if needed. This is optional if you plan to use this system with a hardline connection. Also be aware that if a WiFi connection is used that this system and the Session Server need to be accessible from that WiFi network.

# Updating the Guest Profile #

There are two scripts that can be run at the local administrative account that will capture and restore the guest profile. Any change that needs to be persistant between guest logins will need to be captured. To accomplish this do the following:

* Start a new session and get to the desktop. 
* Once at the desktop leave the Session Manager running.
* Make any changes required (adding icons to the launcher, changing desktop background image, etc.)
* Once changes are complete, capture the profile using the command in a terminal:
```bash
su <administrative account>
sudo captureprofile
```
This will capture the profile and the changes will be reflected in the next guest login. 

# Restoring a Guest Profile #

To restore to a previous Guest profile do the following:

* Start a new session and get to the desktop. 
* Open a terminal and run the following:
```bash
su <administrative account>
sudo restoreprofile
```
A list of previously captured profiles will be listed by date. Enter the number of the profile you wish to restore and hit <Enter>. This will restore the guest profile and those changes will be reflected in the next guest login.
